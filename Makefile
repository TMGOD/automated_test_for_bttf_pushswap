##
## HUGO PAYET, 2022
## Automated_test_for_bttf_push_swap
## File description:
## Makefile
##

SRC = push_swap.c

NAME = push_swap

FLAG = -W -Wall -Wextra -Werror

SRC_ORIGINE = permitted_function.c

NAME_ORIGINE = permitted_function

FILE_ORIGIN = output

FILE_FUNC = expected_function

all :
	gcc -o $(NAME_ORIGINE) $(SRC_ORIGINE) $(FLAG)
	gcc -o $(NAME) $(SRC)
	
glibc : 
	nm ./$(NAME_ORIGINE) | grep GLIBC >$(FILE_ORIGIN)
	nm ./$(NAME) | grep GLIBC >$(FILE_FUNC)
	diff $(FILE_FUNC) $(FILE_ORIGIN)

clean :
	rm -f $(NAME) 
	rm -f $(NAME_ORIGINE)
	rm -f $(FILE_FUNC) 
	rm -f $(FILE_ORIGIN)