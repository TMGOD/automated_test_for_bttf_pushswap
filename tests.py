#!/usr/bin/env -S python3
import unittest
import sys
from io import StringIO
from unittest.mock import patch
import command_checker

class Test_Makefile(unittest.TestCase):
    def test_make_check(self):
        self.assertEqual(command_checker.make(), 0)

    def test_make_glibc(self):
        self.assertEqual(command_checker.make_glibc(), 0)


if __name__ == '__main__':
    unittest.main()