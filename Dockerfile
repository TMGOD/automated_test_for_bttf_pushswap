FROM python:3.10
WORKDIR /automated-test-for-bttf_push_swap
COPY . /test
RUN pip install -r requirements.txt
EXPOSE 80
ENV NOM Hugo
CMD ["python", "command_checker.py"]