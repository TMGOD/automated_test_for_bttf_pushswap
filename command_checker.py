#!/usr/bin/env -S python3
import subprocess
import sys

def make():
    process = subprocess.run(["make"])
    if process.returncode != 0:
        sys.exit(84)
    return 0

def make_glibc():
    process = subprocess.Popen(["make", "glibc"])
    process.wait()
    if process.returncode != 0:
        return 84
    return 0

def make_clean():
    process = subprocess.Popen(["make", "clean"])
    process.wait()
    if process.returncode != 0:
        return 84
    return 0

def main():
    make()
    make_glibc()
    make_clean()

if __name__ == '__main__':
    main()